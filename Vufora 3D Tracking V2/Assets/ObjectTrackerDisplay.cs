﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObjectTrackerDisplay : MonoBehaviour {
    Text[] texts;
    public GameObject blockGuy;
    public GameObject viewMaster;
    // Use this for initialization
    void Start () {
        texts= GetComponentsInChildren<Text>(true);
    }
	
	// Update is called once per frame
	void Update () {
        if (blockGuy.GetComponent<FoundObject>().ObjectFound)
        {
            texts[0].text = "Block Guy = Found";
            texts[0].color = Color.green;
        }
        else
        {
            texts[0].text = "Block Guy = Lost";
            texts[0].color = Color.red;
        }
        if (viewMaster.GetComponent<FoundObject>().ObjectFound)
        {
            texts[1].text = "View-Master = Found";
            texts[1].color = Color.green;
        }
        else
        {
            texts[1].text = "View-Master = Lost";
            texts[1].color = Color.red;
        }
    }
}
